package com.example.batchprocessing;

import lombok.Data;
import lombok.ToString;

/**
 * Excel的字段些
 *
 * @author zhouyue01
 * @date 2021/7/1
 */
@Data
@ToString
public class ExcelFieldEntity {
    private String bid;
    private String loanKind;
    private String loanSide;
    private String guarantee;
    private String loanAmount;
    private String period;
    private String productModel;

}
