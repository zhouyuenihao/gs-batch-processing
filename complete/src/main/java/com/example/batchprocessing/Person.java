package com.example.batchprocessing;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Person {

    private String lastName;
    private String firstName;

    public Person() {
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "firstName: " + firstName + ", lastName: " + lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return lastName.equals(person.lastName) &&
                firstName.equals(person.firstName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName);
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<Object> cf1 = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("一号：" + Thread.currentThread().getName());
            throw new NullPointerException();
            //return null;
        });
        CompletableFuture<Object> cf2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("二号：" + Thread.currentThread().getName());
            throw new IllegalArgumentException();
        });
        CompletableFuture<Object> cf3 = CompletableFuture.supplyAsync(() -> {
            System.out.println("三号：" + Thread.currentThread().getName());
            throw new IllegalMonitorStateException();
        });
        CompletableFuture<Void> all = CompletableFuture.allOf(cf3, cf1, cf2);
        CompletableFuture<Void> complete = all.whenComplete((v, e) -> {
            if (e != null) {
                System.out.println(Thread.currentThread().getName());
            }
        });
        complete.get();

    }

}
