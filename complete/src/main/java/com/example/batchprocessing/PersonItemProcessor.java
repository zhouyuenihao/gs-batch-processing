package com.example.batchprocessing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.item.ItemProcessor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PersonItemProcessor implements ItemProcessor<ExcelFieldEntity, Person> {

	private static final Logger log = LoggerFactory.getLogger(PersonItemProcessor.class);
	private final Map<ExcelFieldEntity, Object> map = new ConcurrentHashMap<>();
	private Object object = new Object();


	@Override
	public Person process(final ExcelFieldEntity in) throws Exception {
		System.out.println("读出来："+in.toString());
		if (map.containsKey(in)) {
			return null;
		}

		final String firstName = in.getLoanKind().toUpperCase();
		final String lastName = in.getLoanSide().toUpperCase();

		final Person transformedPerson = new Person(firstName, lastName);

		log.info("Converting (" + in + ") into (" + transformedPerson + ")");
		map.putIfAbsent(in, object);
		return transformedPerson;
	}


}
